package selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

public class Base {

    static final String url = "https://www.google.com/";
    public static WebDriver driver;

    @BeforeClass
    public static void driverSetup() {
        System.setProperty("webdriver.firefox.driver", "geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Before
    public void setup() {
        driver.get(url);
        String currentUrl = driver.getCurrentUrl();
        assertEquals(url, currentUrl);
        GoogleMainPage googleMainPage = new GoogleMainPage(driver);
        googleMainPage.googleMainPageLogo.isDisplayed();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}