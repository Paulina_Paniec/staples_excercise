package selenium;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static junit.framework.TestCase.assertEquals;

public class StaplesMainPage extends PageObject {

    public StaplesMainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "input.button.js-vatOption.VATbtn")
    public WebElement showPriceInclVatButton;

    @FindBy(css = ".hdr_logo")
    public WebElement staplesMainPageLogo;

    public void clickInclVatButton() {
        waitUntilElementIsClickable(showPriceInclVatButton);
        showPriceInclVatButton.click();
    }

    public void assertLogoLocation(String referenceLocation){
        Point location = staplesMainPageLogo.getLocation();
        assertEquals(referenceLocation, location.toString());
    }

}
