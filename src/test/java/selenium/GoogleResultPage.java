package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleResultPage extends PageObject {

    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "logo")
    public WebElement googleResultPageLogo;

    @FindBy(id = "resultStats")
    public WebElement resultsInformation;

    @FindBy (xpath = "//a[@href='https://www.staples.co.uk/']")
    public WebElement redirectionToStaplesPageElement;

    public void clickStaplesRedirectionLink() {
        waitUntilElementIsClickable(redirectionToStaplesPageElement);
        redirectionToStaplesPageElement.click();
    }
}
