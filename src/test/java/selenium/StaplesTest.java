package selenium;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class StaplesTest extends Base {

    String keyword = "staples solutions uk";

    @Test
    public void googleSearchTest() throws IOException {

        GoogleMainPage googleMainPage = new GoogleMainPage(driver);
        googleMainPage.provideSearchedText(keyword);
        googleMainPage.clickGooglePromptPanelSearchButton();

        GoogleResultPage googleResultPage = new GoogleResultPage(driver);
        googleResultPage.waitUntilElementIsVisible(googleResultPage.googleResultPageLogo);

        String resultsInformationText= googleResultPage.resultsInformation.getText();
        String regexRemoveTextWithBrackets = "\\(([^)]+)\\)";
        String amountOfResults = resultsInformationText.replaceAll(regexRemoveTextWithBrackets, "");
        String regexRemoveNonDigitCharacters = "\\D";
        amountOfResults = amountOfResults.replaceAll(regexRemoveNonDigitCharacters, "");
        System.out.println(amountOfResults);
        googleResultPage.clickStaplesRedirectionLink();

        StaplesMainPage staplesMainPage = new StaplesMainPage(driver);
        staplesMainPage.clickInclVatButton();
        String referenceLogoLocation = "(0, 0)";
        staplesMainPage.assertLogoLocation(referenceLogoLocation);

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("c:\\screenshot.png"));
    }
}